var error_msg="something went wrong"

cat /etc/apache2/httpd.conf >> /etc/apache2/apache2.conf
mkdir /opt/wsconfig
cp /opt/coldfusion9/runtime/lib/wsconfig.jar /opt/wsconfig
cd /opt/wsconfig || exit && echo error_msg
unzip wsconfig.jar
cd /opt/wsconfig/connectors/src || exit && echo error_msg
cp mod_jrun22.c mod_jrun24.c
sed -i 's/remote_addr/client_addr/g' mod_jrun24.c
apxs -ic -n jrun mod_jrun24.c jrun_maptable_impl.c jrun_property.c jrun_session.c platform.c jrun_mutex.c jrun_proxy.c jrun_utils.c
mv /usr/lib/apache2/modules/mod_jrun24.so /opt/coldfusion9/runtime/lib/wsconfig/1/
find /etc/apache2 -type f -exec sed -i 's/mod_jrun22/mod_jrun24/g' {} +
# https://g0blin.co.uk/mod_jrun-on-apache-2-4-ubuntu-14-04-coldfusion-9/

mkdir /var/www/html/maintenance
touch /var/www/html/maintenance/maintenance.cfm
# add custom headers & rewrite rules!
# configure coldfusion admin portal!
# configure the database!
