FROM ubuntu:14.04

WORKDIR /working

EXPOSE 80/tcp
EXPOSE 443/tcp

COPY httpd.conf /working/httpd.conf
COPY .htaccess /working/.htaccess
COPY provision.sh /working/provision.sh
COPY coldfusion9_linux64.bin /working/coldfusion9_linux64.bin

RUN apt-get update && \
    apt-get install software-properties-common -y && \
    add-apt-repository ppa:openjdk-r/ppa && \
    apt-get update && \
    apt-get -y install libstdc++5 vim unzip apache2 apache2-dev build-essential unzip openjdk-8-jdk && \
    a2enmod headers alias rewrite env cgid && \
    mv /working/httpd.conf /etc/apache2/httpd.conf
