## local instance of coldfusion apply

### configure, build, & run

+ add properties.cfm to ~/local-apply/apply/ __*(!do not check this file in!)*__

+ get apply app (&branch)
```shell script
cd ~/local-apply
git clone git@bitbucket.org:jobapp/apply.git && git co <branch_name>
```

+ build/start docker container
```shell script
cd ~/local-apply
docker-compose up
```

+ rebuild docker image
```shell script
cd ~/local-apply
docker-compose build || docker-compose up --build
```

+ stop docker container
```shell script
docker stop cfapply-container
```

#### utility
##### docker ssh
+ ensure `entrypoint: ["tail", "-f", "/dev/null"]` exists in `docker-compose.yml`
+ ssh with `docker exec -it cfapply-container /bin/bash`

##### docker copy 
+ to `docker cp /path/to/dir/file.ext cfapply-container:/path/to/dir`
+ from `docker cp cfapply-container:/path/to/dir/file.ext .`

##### docker purge 
+ remove all images `docker rmi $(docker images)`
+ remove all containers `docker rm $(docker ps -a)`
+ remove all everything `docker system prune`
    - alt: docker desktop -> preferences -> troubleshoot -> clean/purge data

##### debug container
+ `docker container inspect cfapply-container`

### configuring coldfusion manually after build

#### installation
*automate this?*

+ [ssh into container](#docker-ssh)
+ agree to eula
+ install type -> *2- 30-day-trial*
+ installer configuration -> *1- server configuration*
+ is adobe coldfusion 9 (server configuration) installed? -> *2- no*
+ subcomponent installation -> *4) continue with installation (documentation, solr, start cf on sys init are selected)*
+ choose install folder -> *use default install folder*
+ earlier versions of adobe coldfusion installed -> *2- no*
+ configure web servers -> *1- add web server configuration*
+ what kind of web server are you configuring -> *1- apache*
+ what directory contains your apache configuration file (httpd.conf) -> */etc/apache2*
+ where is the apache program binary file -> */usr/sbin/apache2*
+ where is the control file that you use to start and stop the apache web server -> */usr/sbin/apache2ctl*
+ please configure your web server(s) -> *4- continue with installation*
+ choose adobe coldfusion 9 administrator location -> */var/www/html*
+ enter the name of the runtime user. this user must already exist on the system. -> *www-data*
+ configure coldfusion with openoffice -> *2- skip*
+ admin password -> *admin*
+ enable rds  (y/n) -> *y*
+ rds password -> *admin*
+ confirm
+ __exit the installer!__

#### start cf & configure apache
+ start coldfusion 
`/opt/coldfusion9/bin/coldfusion start`
+ configure apache
`cd /working && ./provision.sh`
+ add content from `/working/.htaccess` to `/etc/apache2/apache2.config` within `<Directory /var/www></Directory`
+ restart apache web server `service apache2 restart`
+ view logs
```shell script
cd /opt/coldfusion9/logs
tail -f /opt/coldfusion9/logs/*.log & tail -f /var/log/apache2/*.log
````
+ configure cf server -> goto localhost/cfide/administrator/enter.cfm
+ add datasource via admin panel -> data & services -> data sources
